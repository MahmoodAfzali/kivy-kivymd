import os
import random
import kivy
from kivy.app import App
from kivymd.uix.relativelayout import MDRelativeLayout
from kivymd.uix.button import MDIconButton
from kivymd.app import MDApp
from kivy.core.audio import SoundLoader
from kivy.core.window import Window


Window.size = (350, 600)


class MyApp(MDApp):
    def build(self):

        layout = MDRelativeLayout(md_bg_color = [0, 0.5, 1, 1])

        self.music_dir = "/home/oldman/PycharmProjects/kivu-music"
        self.music_files = os.listdir(self.music_dir)
        self.song_list = [x for x in self.music_files if x.endswith(('mp3'))]
        self.song_count = len(self.song_list)

        self.playbutton = MDIconButton(icon="play-circle-outline", pos_hint={'center_x': .4, 'center_y': .05}, on_press = self.playaudio)
        self.stopbutton = MDIconButton(icon="stop-circle-outline", pos_hint={'center_x': .55, 'center_y': .05}, on_press = self.stopaudio)

        layout.add_widget(self.playbutton)
        layout.add_widget(self.stopbutton)

        return layout

    def playaudio(self, obj):
        self.song_title = self.song_list[random.randrange(0, self.song_count)]
        self.sound = SoundLoader.load('{}/{}'.format(self.music_dir, self.song_title))

        self.sound.play()

    def stopaudio(self, obj):
        self.sound.stop()

if __name__ == '__main__':
    MyApp().run()